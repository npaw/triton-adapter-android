## [6.7.7] - 2022-05-03
### Modified
- Set minimum sdk version to 21.
### Updated
- Update demo
### Added
- Triton 3.2.5 support.
- Add `exoplayer-core:2.16.1` dependency to collect `audioCodec` & `bitrate` at the beginning of a content. 
- Add `MediaPlayer.OnAnalyticsReceivedListener`.

## [6.7.6] - 2022-05-02
### Modified
- Monitor replaced by monitor buffer & seek listeners.
- Set minimum sdk version to 16.
- Set target sdk version to 32.
### Updated
- Update lib version to 6.7.67.
- Update demo
### Added
- Triton 3.1.1 support.

## [6.7.5] - 2021-04-08
### Modified
- Buffer listeners replaced by monitor.

## [6.7.4] - 2021-03-18
### Modified
- fireFatalError replaced by fireError.

## [6.7.3] - 2021-03-12
### Modified
- Deployment platform, moved from Bintray to JFrog.
- Seek listeners replaced by monitor.

## [6.7.2] - 2021-02-11
### Added
- TritonStandaloneAdsAdapter.

## [6.7.1] - 2021-02-04
### Improved
- Ad detection.

## [6.7.0] - 2020-11-16
- First release.