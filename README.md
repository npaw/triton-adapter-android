# Triton Player's Adapter Android

## Documentation, installation & usage

Add this to your build.gradle:

```groovy
repositories {
    ...
    maven { url  "https://npaw.jfrog.io/artifactory/youbora/" }
    ...
}

dependencies {
    ...
    implementation "com.nicepeopleatwork:triton-adapter:6.7.x"
    // x means the latest version available, which is the one we recommend.
    ...
}
```

Also please refer to [Developer Portal](http://developer.nicepeopleatwork.com).

## I need help!
If you find a bug, have a suggestion or need assistance, please send an e-mail to <support@nicepeopleatwork.com>.

