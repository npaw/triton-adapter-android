import os
import json
import shutil

"""
This script builds the folder structure ready to be uploaded to the CDN.
Before running this script, the following gradle tasks must have been run:
- gradle assemble
- gradle createManifest

The script should be invoked from the project root directory.
'python make_deployable.py'
"""

# Paths and file names
# Modify these values for each project
manifest_file_path = "manifest.json"
deployable_name = "triton"
sample_module_name = "sample"
lib_module_name = deployable_name + "-adapter"
apk_name = sample_module_name + "-debug.apk"
aar_name = lib_module_name + "-release.aar"
# package_type, "adapters" for adapters, or "" (empty string) for libraries
package_type = "adapters"

version = None

with open(manifest_file_path) as manifest_file:
    try:
        jsonManifest = json.load(manifest_file)
        version = jsonManifest["version"]
    except:
        version = None

if version is None:
    print("Couldn't get the version from the manifest, aborting (have you run 'gradle createManifest'?).")
    exit(-1)

last_build_path = "deploy/last-build/" + package_type + "/" + deployable_name + "/last-build"
version_path = "deploy/version/" + package_type + "/" + deployable_name + "/" + version
# sample_dest_path = last_build_path + "/sample"

# Create folder structure
if os.path.isdir("deploy"):
    shutil.rmtree("deploy")

os.makedirs(last_build_path)

# Copy manifest, demo, apk and aar
print("Copying manifest...")
shutil.copy(manifest_file_path, last_build_path)

# ZIP
"""
print "Copying sample module..."
shutil.copytree(sample_module_name, sample_dest_path + "/" + sample_module_name)
print "Removing build path..."
shutil.rmtree(sample_dest_path + "/" + sample_module_name + "/build")
print "Zipping sample..."
shutil.make_archive(deployable_name + "-sample", "zip", sample_dest_path)
print "Moving sample..."
shutil.move(deployable_name + "-sample.zip", last_build_path)
print "Removing sample folder copy..."
shutil.rmtree(sample_dest_path)
"""

print("Copying apk...")
shutil.copy(sample_module_name + "/build/outputs/apk/debug/" + apk_name, last_build_path)
print("Copying binary...")
shutil.copy(lib_module_name + "/build/outputs/aar/" + aar_name, last_build_path)
# Copy over to version
print("Copying last-build to " + version + "...")
shutil.copytree(last_build_path, version_path)

print("Done!")