package com.npaw.youbora.lib6.triton

import android.os.Bundle
import com.npaw.youbora.lib6.Timer
import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.adapter.PlayerAdapter
import com.tritondigital.player.MediaPlayer
import com.tritondigital.player.TritonPlayer

open class TritonAdapter @JvmOverloads constructor(
        player: TritonPlayer, private val isAdsAdapterEnabled: Boolean = true
) : PlayerAdapter<TritonPlayer>(player) {

    private var lastTitle: String? = null
    private var lastTrackId: String? = null
    private var lastArtistName: String? = null
    private var lastAudioCodec: String? = null
    private var lastBitrate: Long? = -1L

    var onAnalyticsReceivedListener : MediaPlayer.OnAnalyticsReceivedListener? = null
    var onStateChangedListener: MediaPlayer.OnStateChangedListener? = null
    var onInfoListener: MediaPlayer.OnInfoListener? = null
    var onCuePointReceivedListener: MediaPlayer.OnCuePointReceivedListener? = null

    private var adsAdapter = TritonAdsAdapter(player)
    private var lastPlayhead = 0.0
    private var playheadZero = 0.0
    internal var adDurationAccumulated = 0.0

    private val onStateChanged = MediaPlayer.OnStateChangedListener { mediaPlayer, state ->
        when (state) {
            TritonPlayer.STATE_CONNECTING -> {
                YouboraLog.debug("TritonPlayer.STATE_CONNECTING")
                fireResume()
                fireStart()
            }
            TritonPlayer.STATE_ERROR -> {
                YouboraLog.debug("TritonPlayer.STATE_ERROR")
                adsAdapter.fireStop()
                fireError(player.errorCode.toString())
            }
            TritonPlayer.STATE_PLAYING -> {
                YouboraLog.debug("TritonPlayer.STATE_PLAYING")
                if (!isAdBreakStarted()) fireJoin()
            }
            TritonPlayer.STATE_COMPLETED, TritonPlayer.STATE_STOPPED -> {
                YouboraLog.debug("TritonPlayer.STATE_COMPLETED or TritonPlayer.STATE_STOPPED")
                fireStop()
            }
            TritonPlayer.STATE_PAUSED -> {
                YouboraLog.debug("TritonPlayer.STATE_PAUSED")
                firePause()
            }
        }
        onStateChangedListener?.onStateChanged(mediaPlayer, state)
    }

    private val onCuePointReceived = MediaPlayer.OnCuePointReceivedListener { mediaPlayer, bundle ->
        YouboraLog.debug("onCuePointReceived: bundle -> $bundle")

        if (bundle?.getString("cue_type") == "ad") {
            onAdStarted(bundle)
        } else if (bundle?.getString("cue_type") == "track") {
            adsAdapter.fireStop()
            adsAdapter.fireAdBreakStop()
            onTrackStarted(bundle)
        }

        onCuePointReceivedListener?.onCuePointReceived(mediaPlayer, bundle)
    }

    private fun onTrackStarted(bundle: Bundle) {
        if (bundle.getString("cue_title") != null)
            lastTitle = bundle.getString("cue_title")

        if (bundle.getString("track_artist_name") != null)
            lastArtistName = bundle.getString("track_artist_name")

        if (bundle.getString("track_id") != null) {
            val currentTrackId = bundle.getString("track_id")
            if (lastTrackId != null && !lastTrackId.equals(currentTrackId)) {
                fireStop()
            }
            lastTrackId = currentTrackId
            fireStart()
        }
    }

    private fun onAdStarted(bundle: Bundle) {
        adsAdapter.fireStop()
        adsAdapter.lastResource = bundle.getString("ad_vast_url")
        adsAdapter.lastDuration = bundle.getInt("cue_time_duration") / 1000.0
        adsAdapter.lastTitle = bundle.getString("cue_title")
        adsAdapter.fireStart()
    }

    private val onInfo = MediaPlayer.OnInfoListener { mediaPlayer, state, value ->

        if (isAdsAdapterEnabled) {
            plugin?.takeIf { it.adsAdapter == null }?.let {
                it.adsAdapter = adsAdapter.apply { registerListeners() }
            }
        }

        if (!adsAdapter.getAdFlags().isAdBreakStarted) {
            when (state) {
                TritonPlayer.INFO_SEEK_COMPLETED -> {
                    //YouboraLog.debug("TritonPlayer.INFO_SEEK_COMPLETED - $value")
                    //fireSeekEnd()
                }
                TritonPlayer.INFO_SEEK_STARTED -> {
                    YouboraLog.debug("TritonPlayer.INFO_SEEK_STARTED - $value")
                    fireSeekBegin()
                }
                TritonPlayer.INFO_BUFFERING_START -> {
                    YouboraLog.debug("TritonPlayer.INFO_BUFFERING_START - $value")
                    fireBufferBegin()
                }
                TritonPlayer.INFO_BUFFERING_COMPLETED -> {
                    YouboraLog.debug("TritonPlayer.INFO_BUFFERING_COMPLETED - $value")
                    fireBufferEnd()
                    fireSeekEnd()
                }
            }
        }

        onInfoListener?.onInfo(mediaPlayer, state, value)
    }

    private val onAnalyticsReceived = MediaPlayer.OnAnalyticsReceivedListener { mediaPlayer, format ->
        if (player == mediaPlayer) {
            lastAudioCodec = format.codecs
            lastBitrate = format.bitrate.toLong()
        }

        onAnalyticsReceivedListener?.onAnalyticsReceivedListener(mediaPlayer, format)
    }



    private var joinTimer = Timer(object : Timer.TimerEventListener {
        override fun onTimerEvent(delta: Long) {
            if (getPlayhead() > playheadZero) fireJoin()
        }
    }, 100)

    init {
        registerListeners()
    }

    override fun registerListeners() {
        super.registerListeners()
        player?.setOnAnalyticsReceivedListener(onAnalyticsReceived)
        player?.onStateChangedListener = onStateChanged
        player?.onInfoListener = onInfo
        player?.onCuePointReceivedListener = onCuePointReceived
        YouboraLog.debug("Video listeners registered")
        plugin?.adsAdapter?.let { if (it is TritonAdsAdapter) it.registerListeners() }
    }

    override fun unregisterListeners() {
        super.unregisterListeners()
        player?.setOnAnalyticsReceivedListener(onAnalyticsReceivedListener)
        player?.onStateChangedListener = onStateChangedListener
        player?.onInfoListener = onInfoListener
        player?.onCuePointReceivedListener = onCuePointReceivedListener
    }

    override fun getResource(): String? {
        var resource = super.getResource()

        try {
            resource = player?.castStreamingUrl
        } catch (e: Exception) {
            YouboraLog.error("An internal error occurred on Triton player when trying to get " +
                    "castStreamingUrl from the player instance")
        }

        return resource
    }

    override fun getIsLive(): Boolean? { return player?.takeIf { it.duration == Integer.MAX_VALUE }.let { true } }
    override fun getPlayerName(): String { return "Triton" }
    override fun getDuration(): Double? { return player?.let { it.duration / 1000.0 } }
    override fun getProgram(): String? { return "$lastTitle - $lastArtistName" }
    override fun getTitle(): String? { return lastTitle }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName() }
    override fun getBitrate(): Long? { return lastBitrate ?: super.getBitrate() }
    override fun getAudioCodec(): String? { return lastAudioCodec ?: super.getAudioCodec() }
    override fun getPlayhead(): Double {
        player.takeIf { !isAdBreakStarted() }?.let {
            lastPlayhead = (it.position / 1000.0) - adDurationAccumulated
        }

        return lastPlayhead
    }

    override fun fireStart(params: MutableMap<String, String>) {
        super.fireStart(params)
        playheadZero = getPlayhead()
        joinTimer.start()
    }

    override fun fireJoin(params: MutableMap<String, String>) {
        joinTimer.stop()
        super.fireJoin(params)
    }

    override fun fireStop(params: MutableMap<String, String>) {
        super.fireStop(params)
        resetValues()
    }

    private fun resetValues() {
        lastTrackId = null
        lastTitle = null
        lastArtistName = null
        lastAudioCodec = null
        lastBitrate = -1L
        lastPlayhead = 0.0
        playheadZero = 0.0
        adDurationAccumulated = 0.0
    }

    private fun isAdBreakStarted(): Boolean {
        return plugin?.adsAdapter?.getAdFlags()?.isAdBreakStarted ?: false
    }
}