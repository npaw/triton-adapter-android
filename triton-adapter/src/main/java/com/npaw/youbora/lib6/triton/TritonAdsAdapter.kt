package com.npaw.youbora.lib6.triton

import com.npaw.youbora.lib6.Timer
import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.adapter.AdAdapter

import com.tritondigital.player.MediaPlayer
import com.tritondigital.player.TritonPlayer

open class TritonAdsAdapter(player: TritonPlayer) : AdAdapter<TritonPlayer>(player) {

    var lastResource: String? = null
    var lastDuration: Double? = null
    var lastTitle: String? = null

    private var lastPlayhead = 0.0
    private var firstPlayhead = 0.0
    private var joinTimer = Timer(object : Timer.TimerEventListener {
        override fun onTimerEvent(delta: Long) {
            if (getPlayhead() != 0.0) fireJoin()
        }
    }, 100)

    override fun fireStart(params: MutableMap<String, String>) {
        player?.let { firstPlayhead = it.position / 1000.0 }
        super.fireStart(params)
        joinTimer.start()
    }

    override fun fireJoin(params: MutableMap<String, String>) {
        joinTimer.stop()
        super.fireJoin(params)
    }

    override fun getTitle(): String? { return lastTitle }
    override fun getPlayerName(): String { return "Triton" }
    override fun getResource(): String? { return lastResource }
    override fun getDuration(): Double? { return lastDuration }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName() }

    override fun getPlayhead(): Double? {
        player.takeIf { getAdFlags().isAdBreakStarted }?.let { p ->
            lastPlayhead = (p.position / 1000.0) - firstPlayhead
        }

        return lastPlayhead
    }

    override fun fireStop(params: MutableMap<String, String>) {
        super.fireStop(params)

        plugin?.adapter?.let { a ->
            if (a is TritonAdapter) {
                val adDuration = lastPlayhead.minus(firstPlayhead)
                a.adDurationAccumulated = a.adDurationAccumulated.plus(adDuration)
            }
        }

        resetValues()
    }

    private fun resetValues() {
        lastResource = null
        lastDuration = null
        lastTitle = null
        lastPlayhead = 0.0
        firstPlayhead = 0.0
    }
}