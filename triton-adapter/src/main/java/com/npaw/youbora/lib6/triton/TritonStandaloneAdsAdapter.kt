package com.npaw.youbora.lib6.triton

import com.npaw.youbora.lib6.Timer
import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.adapter.AdAdapter

import com.tritondigital.player.MediaPlayer
import com.tritondigital.player.TritonPlayer

import java.lang.Exception

open class TritonStandaloneAdsAdapter(player: TritonPlayer) : AdAdapter<TritonPlayer>(player) {

    var onStateChangedListener: MediaPlayer.OnStateChangedListener? = null
    var onInfoListener: MediaPlayer.OnInfoListener? = null

    private var lastPlayhead = 0.0
    private var playheadZero = 0.0

    private val onStateChanged = MediaPlayer.OnStateChangedListener { mediaPlayer, state ->
        YouboraLog.debug("Ads: $state")

        when (state) {
            201 -> {
                fireResume()
                fireStart()
            }

            202 -> fireFatalError(player.errorCode.toString(), null, null)
            203 -> fireJoin()
            200, 205 -> fireStop()
            206 -> firePause()
        }

        onStateChangedListener?.onStateChanged(mediaPlayer, state)
    }

    private val onInfo = MediaPlayer.OnInfoListener { mediaPlayer, state, value ->
        YouboraLog.debug("Ads $state - $value")

        when (state) {
            275 -> fireBufferBegin()
            276 -> fireBufferEnd()
        }

        onInfoListener?.onInfo(mediaPlayer, state, value)
    }

    private var joinTimer = Timer(object : Timer.TimerEventListener {
        override fun onTimerEvent(delta: Long) {
            if (getPlayhead() > playheadZero) fireJoin()
        }
    }, 100)

    init { registerListeners() }

    override fun registerListeners() {
        super.registerListeners()

        player?.onStateChangedListener = onStateChanged
        player?.onInfoListener = onInfo
    }

    override fun unregisterListeners() {
        super.unregisterListeners()

        player?.onStateChangedListener = onStateChangedListener
        player?.onInfoListener = onInfoListener
    }

    override fun getResource(): String? {
        var resource = super.getResource()

        try {
            resource = player?.castStreamingUrl
        } catch (e: Exception) {
            YouboraLog.error("An internal error occurred on Triton player when trying to get " +
                    "castStreamingUrl from the player instance")
        }

        return resource
    }

    override fun getDuration(): Double? {
        return player?.let { it.duration / 1000.0 }
    }

    override fun getPlayhead(): Double {
        player?.let {
            lastPlayhead = it.position / 1000.0
        }

        return lastPlayhead
    }

    override fun getPlayerName(): String { return "Triton" }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName() }

    override fun fireStart(params: MutableMap<String, String>) {
        super.fireStart(params)
        playheadZero = getPlayhead()
        joinTimer.start()
    }

    override fun fireJoin(params: MutableMap<String, String>) {
        joinTimer.stop()
        super.fireJoin(params)
    }

    override fun fireStop(params: MutableMap<String, String>) {
        super.fireStop(params)
        resetValues()
    }

    private fun resetValues() {
        lastPlayhead = 0.0
        playheadZero = 0.0
    }
}